const express = require('express');
const router = express.Router();

// Require the controllers WHICH WE DID NOT CREATE YET!!
const trip_controller = require('../controllers/trip.controller');


// a simple test url to check that all of our files are communicating correctly.

router.post('/updateTripRemark', trip_controller.update_trip_remark);
router.post('/getTripsByBeaconIdList', trip_controller.get_trip_by_beacon_ids);
router.post('/getTripsByDept', trip_controller.get_trip_by_dept);
router.post('/getTripsByTripID', trip_controller.get_trip_by_trip_id);


module.exports = router;