const express = require('express');
const router = express.Router();

// Require the controllers WHICH WE DID NOT CREATE YET!!
const beacon_controller = require('../controllers/beacon.controller');


// a simple test url to check that all of our files are communicating correctly.
router.get('/test', beacon_controller.test);
router.get('/init', beacon_controller.beacon_init);
router.get('/create', beacon_controller.beacon_create);
router.get('/check', beacon_controller.beacon_check);
router.post('/update', beacon_controller.beacon_update);
router.get('/test_group', beacon_controller.beacon_test_group);
router.get('/lastseen_check', beacon_controller.beacon_lastseen_check_web);

router.post('/getBeaconsByDept', beacon_controller.get_beacons_by_dept);


module.exports = router;