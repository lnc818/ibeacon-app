var beaconsInit=[
    {
        id: '00000001',
        name: 'iBeacon1',
        from_dept: '',
        rssi: -80,
        current_location: '',
        last_seen_location: '',
        join_date: new Date(),
        last_seen_date: new Date(),
        request_change_location: 0
    },
    {
        id: '00000002',
        name: 'iBeacon2',
        from_dept: '',
        rssi: -80,
        current_location: '',
        last_seen_location: '',
        join_date: new Date(),
        last_seen_date: new Date(),
        request_change_location: 0
    },
    {
        id: '00000003',
        name: 'iBeacon3',
        from_dept: '',
        rssi: -80,
        current_location: '',
        last_seen_location: '',
        join_date: new Date(),
        last_seen_date: new Date(),
        request_change_location: 0
    },

    
]

module.exports.getInitBeacons = function (){ return beaconsInit};