var trays=[
    {
        dept:"AA",
        IN:
        {
            name:"AA_IN",
            beacon_count: 0
        },
        OUT:
        {
            name:"AA_OUT",
            beacon_count: 0
        }
    },
    {
        dept:"BB",
        IN:
        {
            name:"BB_IN",
            beacon_count: 0
        },
        OUT:
        {
            name:"BB_OUT",
            beacon_count: 0
        }
    },
    {
        dept:"CART",
        name:"CART",
        beacon_count: 0
    }
]

module.exports.getInitTrays = function (){ return trays};