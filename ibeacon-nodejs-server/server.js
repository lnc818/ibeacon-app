var express = require('express');
var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var mongoose = require('mongoose');
const bodyParser = require('body-parser');
var path = require('path');

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/beaconServer', { useMongoClient: true });
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));



const beacon = require('./routes/beacon.route');
const trip = require('./routes/trip.route');

app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use('/beacons', beacon);
app.use('/trips', trip);

const beacon_controller = require('./controllers/beacon.controller');


const port = 8081
server.listen(port, () => {
  console.log(`Listening on port ${port}`);
  let lastseen_check = setInterval(beacon_controller.beacon_lastseen_check, 6000);}
)


app.get('/', function (req, res) {
  res.sendFile(__dirname + '/public/index.html');
});
app.get('/dept', function (req, res) {
  res.sendFile(__dirname + '/public/dept.html');
});


io.on('connection', function (socket) {
  console.log("connected!");
  socket.emit('connected', { message: 'connected' });
  socket.on('on_connected', function (data) {
    console.log(data);
  });
});

