const Beacon = require('../models/beacon.model');
const BeaconHistory = require('../models/beaconHistory.model');
const Trip = require('../models/Trip.model');
const TripDetail = require('../models/TripDetail.model');
var moment = require('moment');


exports.beacon_init = async function (req, res) {
    const beaconsInit = require('../config/beaconsInit');
    await Beacon.remove({});
    await Trip.remove({});
    await TripDetail.remove({});

    const beacons_init = beaconsInit.getInitBeacons();
    beacons_init.forEach(b => {
        let beacon = new Beacon({...b});
        beacon.save(function (err) {
            if (err) {
                res.send('Beacon Init Failed!')
                return next(err);
            }
        })
    });
    res.send('Beacon Created successfully');
}
exports.beacon_create = function (req, res) {
    let beacon = new Beacon(
        {
            id: '0081f96211d9',
            name: 'IT Beacon 2',
            rssi: -80,
            current_location: '',
            last_seen_location: '',
            join_date: new Date(),
            last_seen_date: new Date(),
            request_change_location: 0
        }
    );

    beacon.save(function (err) {
        if (err) {
            return next(err);
        }
        //res.send('Beacon Created successfully')
    })

    beacon = new Beacon(
        {
            id: '0081f9621506',
            name: 'IT Beacon 1',
            rssi: -80,
            current_location: '',
            last_seen_location: '',
            join_date: new Date(),
            last_seen_date: new Date(),
            request_change_location: 0
        }
    );
    beacon.save(function (err) {
        if (err) {
            return next(err);
        }
        //res.send('Beacon Created successfully')
    })
    res.send('Beacon Created successfully');
};

exports.beacon_check = function (req, res) {
    Beacon.findOne({}).then(beacon => {
        if (beacon) {
          console.log(beacon);
          res.send(beacon);
        }
    
        res.send("empty");
      });
};

exports.beacon_update = async function (req, res) {

    var beaconlist = req.body.beacons;
    var station = req.body.station;
    var station_type = req.body.station_type;
    var station_dept = req.body.station_dept;
    var timeNow = new Date();

    for(let beacon of beaconlist){
        let beacon_db = await Beacon.findOne({id: beacon.id});
            if (beacon_db) {
                console.log(station);
                console.log(beacon_db);
                //If Beacon last seen location is same as current location, only update the rssi and last_seen_date
                if(beacon_db.last_seen_location == station)
                {
                    Beacon.update({ id: beacon.id }, { rssi: beacon.rssi, current_location: station, last_seen_date: timeNow, request_change_location: 0 }).then(()=>{
                        console.log("updated");
                    });
                }
                //else update beacon location
                else
                {
                    if(beacon_db.request_change_location < 2)
                    {
                        var request_change_location = beacon_db.request_change_location + 1;
                        Beacon.update({ id: beacon.id }, 
                            { 
                                request_change_location: request_change_location
                            }
                        )
                            .then(()=>{
                            console.log("Beacon updated");
                        });
                    }
                    else
                    {
                        //if beacon goes to a new 'OUT' station, new trip is spawned
                        if(station_type == 'OUT')
                        {
                            let timeNow = new Date();

                            if(beacon_db.current_trip != null && beacon_db.current_trip != undefined && beacon_db.current_trip != '')
                            {
                                let currentTrip = await Trip.findOne({_id: beacon_db.current_trip});
                                await Trip.update({_id: currentTrip},
                                    { 
                                        is_completed: true,
                                        has_error: true,
                                        error_message: 'Beacon reused before this trip is completed',
                                        //creation_date: timeNow,
                                        last_modified_date: timeNow,
                                    }
                                );
                            }
                            
                            let trip_no = await Trip.count({});
                            trip_no++;

                            let trip = new Trip(
                                {
                                    // id: {type: String, required: true, max: 200},
                                    trip_no: 'T' + trip_no + '-' + timeNow.toLocaleDateString("en-US"),
                                    trip_remark: '',
                                    beacon_id: beacon_db.id,
                                    beacon_name: beacon_db.name,
                                    from_dept: station_dept,
                                    from: station,
                                    currentStatus: 'OUT',
                                    is_completed: false,
                                    has_error: false,
                                    error_message: '',
                                    creation_date: timeNow,
                                    last_modified_date: timeNow,
                                }
                            );
                            trip.save(function (err,t) {
                                if (err) {
                                    console.log("trip insertion failed!")
                                }
                                console.log("trip insert successfully!");

                                Beacon.update({ id: beacon.id }, 
                                    { 
                                        from_dept: station_dept,
                                        rssi: beacon.rssi,
                                        current_trip: t.id,
                                        current_location: station,
                                        last_seen_location: station,
                                        join_date: timeNow, 
                                        last_seen_date: timeNow ,
                                        request_change_location: 0,
                                    }
                                )
                                    .then(()=>{
                                    console.log("Beacon updated");
                                });

                                let tripDetail = new TripDetail(
                                   {
                                        trip_id: t._id,
                                        beacon_id: beacon_db.id,
                                        beacon_name: beacon_db.name,
                                        station: station,
                                        station_dept: station_dept,
                                        status: station_type,
                                        creation_date: timeNow,
                                   }
                                )
                                tripDetail.save(function (err,td) {
                                    if (err) {
                                        console.log("trip detail insertion failed!")
                                    }
                                    console.log("trip detail insert successfully!");
                                });
                            })
                        }
                        else
                        {
                            let timeNow = new Date();
                            let trip = await Trip.findOne({_id: beacon_db.current_trip});

                            let is_completed = false;
                            if(trip != null && trip != undefined)
                            {
                                if(trip.is_completed && station_type == 'IN')
                                    is_completed = true;
                                await Trip.update({_id: beacon_db.current_trip},
                                    {
                                        currentStatus: station_type,
                                        is_completed: is_completed,
                                        has_error: false,
                                        error_message: '',
                                        last_modified_date: timeNow,
                                    }
                                );

                                let tripDetail = new TripDetail(
                                    {
                                         trip_id: trip._id,
                                         beacon_id: beacon_db.id,
                                         beacon_name: beacon_db.name,
                                         station: station,
                                         station_dept: station_dept,
                                         status: station_type,
                                         creation_date: timeNow,
                                    }
                                )
                                await tripDetail.save();
                            }
                            if(is_completed)
                            {
                                await Beacon.update({ id: beacon.id }, 
                                    { 
                                        from_dept: '',
                                        rssi: beacon.rssi,
                                        current_location: station,
                                        last_seen_location: station,
                                        current_trip: undefined,
                                        join_date: timeNow, 
                                        last_seen_date: timeNow ,
                                        request_change_location: 0,
                                    }
                                )
                            }
                            else
                            {
                                await Beacon.update({ id: beacon.id }, 
                                    { 
                                        rssi: beacon.rssi,
                                        current_location: station,
                                        last_seen_location: station,
                                        join_date: timeNow, 
                                        last_seen_date: timeNow ,
                                        request_change_location: 0,
                                    }
                                )
                            }
                        }
                    }
                }
            }
        // })
    }
    res.send("empty");
};
exports.beacon_lastseen_check = async function(){
    var checkTime = moment().subtract(15, 'seconds').toDate();
    
    let beacons = await Beacon.find({ last_seen_date: {
        $lte: checkTime
      } });

    console.log(beacons);

    let timeNow = new Date();

    ////if beacons leave for > 15 seconds after arriving 'IN' station, we send that trip is completed!
    beacons.forEach(async (b) => {
        if(b.current_trip != null && b.current_trip != undefined && b.current_trip != '')
        {
            let trip = await Trip.findOne({_id: b.current_trip});
            if(trip.currentStatus == 'IN')
            {
                await Trip.update({_id: b.current_trip},
                    {
                        is_completed: true,
                        has_error: false,
                        last_modified_date: timeNow,
                    }
                );
                await Beacon.update({id: b.id},{
                    current_trip: undefined,
                    last_seen_date: timeNow,
                })
            }
        }
    });
    ////

    Beacon.update({ last_seen_date: {
        $lte: checkTime
      } }, { rssi: -100, current_location: '' }, {multi: true})
      .then(()=>{
        console.log("lastseen_check");
    });
}
exports.beacon_lastseen_check_web = function (req, res) {
    var checkTime = moment().subtract(15, 'seconds').toDate();
    
    Beacon.update({ last_seen_date: {
        $lte: checkTime
      } }, { rssi: -100, current_location: '' }, {multi: true})
      .then(()=>{
        console.log("lastseen_check");
    });
    Beacon.find({ last_seen_date: {
        $lte: checkTime
      } }).then(beacon_db => {
        console.log(beacon_db);
        res.send(beacon_db)
    });
};

exports.beacon_test_group = function(req,res){
    const trays = require('../config/trays');
    Beacon.aggregate(
        [
            { "$sort": { "id": 1 } },
            { "$group": { 
                _id : "$current_location",
                count : { $sum : 1 },
            }}
        ],
        function(err,results) {
            if (err) 
            {
                throw err;
            }

            
            var traylist = trays.getInitTrays();
            results.forEach(item => {
                traylist = traylist.map(tray =>{ 
                    if(tray.dept == "CART")
                    {
                        if(tray.name == item._id){
                            return {
                                ...tray, 
                                name: item._id,
                                beacon_count: item.count
                            }
                        }
                        else
                        {
                            return tray;
                        }
                    }
                    else if(tray.IN.name == item._id)
                    {
                        return {
                            ...tray, 
                            IN: 
                            {
                                name: item._id,
                                beacon_count: item.count
                            }
                        }
                    }
                    else if (tray.OUT.name == item._id)
                    {
                        return {
                            ...tray, 
                            OUT: 
                            {
                                name: item._id,
                                beacon_count: item.count
                            }
                        }
                    }
                    else
                        return tray;
                 });
            });
            console.log( traylist);
            res.send({results: results, traylist: traylist});
        }
    )
}

exports.get_beacons_by_dept = async function(req,res){
    let dept = req.body.dept;
    if(dept != null && dept!= undefined)
    {
        let beacons_in = await Beacon.find({current_location: dept+'_IN'}).populate({
            path: 'current_trip',
            match: { is_completed: false }
        }).sort({join_date: 1});
        let beacons_out = await Beacon.find({current_location: dept+'_OUT'}).populate({
            path: 'current_trip',
            match: { is_completed: false }
        }).sort({join_date: 1});
        let beacons_on_the_fly = await Beacon.find(
            {
                from_dept: dept, 
                current_trip: { "$ne": null }, 
                current_location: {"$nin": [dept+'_IN', dept+'_OUT']}
            }
        ).populate({
            path: 'current_trip',
            //select: 'currentStatus',
            match: { is_completed: false }
        }).sort({last_seen_date: -1});

        let beacons_completed = await Trip.aggregate([
            {
                $match: {
                    from_dept: dept,
                    is_completed: true
                } // your match filter criteria
            },
            {
                $sort: {
                    creation_date: -1
                }
            },
            { 
                $group: { 
                    _id: '$beacon_id',
                    trip_no: {$first: '$trip_no'},
                    trip_id: { $first: '$_id' },
                    trip_remark: { $first: '$trip_remark' },
                    beacon_name: { $first: '$beacon_name' },
                    is_completed: { $first: '$is_completed' },
                    has_error: { $first: '$has_error' }
                }
            },
            {
                $project: {
                    beacon_id: '$_id',
                    beacon_name: '$beacon_name',
                    trip_no: '$trip_no',
                    trip_id: '$trip_id',
                    trip_remark: '$trip_remark',
                    is_completed: '$is_completed',
                    has_error: '$has_error'
                }
            }
          ])
        res.json({ beacons_in: beacons_in, beacons_out: beacons_out, beacons_on_the_fly: beacons_on_the_fly, beacons_completed: beacons_completed });
    }
}
