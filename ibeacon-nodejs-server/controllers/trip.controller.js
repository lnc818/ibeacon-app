const Beacon = require('../models/beacon.model');
const BeaconHistory = require('../models/beaconHistory.model');
const Trip = require('../models/Trip.model');
const TripDetail = require('../models/TripDetail.model');
var moment = require('moment');


exports.update_trip_remark = async function(req, res){
    let trip_id = req.body.trip_id;
    let trip_remark = req.body.trip_remark;
    if(trip_id != null && trip_id!= undefined)
    {
        let trip = await Trip.update({_id:trip_id},{trip_remark: trip_remark});
        res.json({ message: 'Trip remark is updated! ', isValid: true });
    }
    else
    {
        res.json({ message: 'Trip remark is failed to update!', isValid: false });
    }
}
exports.get_trip_by_beacon_ids = async function(req, res){
    let beacon_id_list = req.body.beacon_id_list;
    let dept = req.body.dept;
    var trip_list = [];
    if(dept != null && dept!= undefined && beacon_id_list != null && beacon_id_list!= undefined)
    {
        for(let beacon_id of beacon_id_list){
            let trip = await Trip.findOne({beacon_id: beacon_id, from_dept: dept}).sort({creation_date: -1}).limit(1);
            if(trip != null && trip != undefined)
            {
                let tripDetails = await TripDetail.find({trip_id:trip._id}).sort({creation_date: 1})
                trip_list.push({...trip._doc,tripDetails:tripDetails});
            }
        }
        
        res.json({ trip_list: trip_list, isValid: true });
    }
    else
    {
        res.json({ trip_list: trip_list, isValid: false });
    }
}
exports.get_trip_by_dept = async function(req, res){
    let dept = req.body.dept;

    var beacon_list = await Beacon.find({from_dept: dept});

    var trip_list = [];
    if(dept != null && dept!= undefined)
    {
        for(let b of beacon_list){
            var beacon_id = b.id; 
            let trip = await Trip.findOne({beacon_id: beacon_id, from_dept: dept}).sort({creation_date: -1}).limit(1);
            if(trip != null && trip != undefined)
            {
                let tripDetails = await TripDetail.find({trip_id:trip._id}).sort({creation_date: 1})
                trip_list.push({...trip._doc,tripDetails:tripDetails});
            }
        }
        
        res.json({ trip_list: trip_list, isValid: true });
    }
    else
    {
        res.json({ trip_list: trip_list, isValid: false });
    }
}
exports.get_trip_by_trip_id = async function(req, res){
    let trip_id = req.body.trip_id;
    if(trip_id != null && trip_id!= undefined)
    {
        let trip = await Trip.findOne({_id:trip_id}).sort({creation_date: -1});
        if(trip != null && trip != undefined)
        {
            let tripDetails = await TripDetail.find({trip_id:trip._id}).sort({creation_date: 1});

            let trip_res =  {...trip._doc, tripDetails:tripDetails};
            res.json({ trip: trip_res, isValid: true });
        }
        else
        {
            res.json({ trip: null, isValid: true });
        }
    }
    else
    {
        res.json({ trip: null, isValid: false });
    }
}