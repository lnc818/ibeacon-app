var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var BeaconHistorySchema = new Schema(
  {
    id: {type: String, required: true, max: 200},
    name: {type: String, required: true, max: 200},
    from: {type: String},
    to: {type: String},
    status: {type: String},
    creation_date: {type: Date},
  }
);


//Export model
module.exports = mongoose.model('BeaconHistory', BeaconHistorySchema);