var mongoose = require('mongoose');
const Trip = require('../models/Trip.model');

var Schema = mongoose.Schema;

var TripDetailSchema = new Schema(
  {
    trip_id: {type: mongoose.Schema.Types.ObjectId, ref: 'Trip'},
    beacon_id: {type: String, required: true, max: 200},
    beacon_name: {type: String, required: true, max: 200},
    station: {type: String},
    station_dept: {type: String},
    status: {type: String},
    creation_date: {type: Date},
  }
);


//Export model
module.exports = mongoose.model('TripDetail', TripDetailSchema);