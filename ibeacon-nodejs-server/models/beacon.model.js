var mongoose = require('mongoose');
const Trip = require('./Trip.model');

var Schema = mongoose.Schema;

var BeaconSchema = new Schema(
  {
    id: {type: String, required: true, max: 200},
    name: {type: String, required: true, max: 200},
    from_dept: {type: String},
    rssi: {type: Number},
    current_trip: {type: mongoose.Schema.Types.ObjectId, ref: 'Trip'},
    current_location: {type: String},
    last_seen_location: {type: String},
    join_date: {type: Date},
    last_seen_date: {type: Date},
    request_change_location: {type: Number},
  }
);

BeaconSchema
.virtual('dept')
.get(function () {
  if(this.current_location == '')
    return '';
  else if(this.current_location.indexOf("_") == -1)
    return 'CART';
  else
    return current_location.split("_")[0];
});

BeaconSchema
.virtual('station_type')
.get(function () {
  if(this.current_location == '')
    return '';
  else if(this.current_location.indexOf("_") == -1)
    return 'CART';
  else
    return current_location.split("_")[1];
});

BeaconSchema
.virtual('age')
.get(function () {
  return (this.last_seen_date.getYear() - this.join_date.getYear()).toString();
});

//Export model
module.exports = mongoose.model('Beacon', BeaconSchema);