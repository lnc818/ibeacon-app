var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var TripSchema = new Schema(
  {
    // id: {type: String, required: true, max: 200},
    trip_no: {type: String},
    trip_remark: {type: String},
    beacon_id: {type: String, required: true, max: 200},
    beacon_name: {type: String, required: true, max: 200},
    from_dept: {type: String},
    from: {type: String},
    currentStatus: {type: String},
    is_completed: {type: Boolean},
    has_error: {type: Boolean},
    error_message: {type: String},
    creation_date: {type: Date},
    last_modified_date: {type: Date},
  }
);


//Export model
module.exports = mongoose.model('Trip', TripSchema);